/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017-2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.erpReports;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.distributionorder.DistributionOrder;
import org.openbravo.erpCommon.utility.reporting.TemplateData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Print_DistOrder extends HttpSecureAppServlet {
  private static final Logger log = LoggerFactory.getLogger(Print_DistOrder.class);
  private static final long serialVersionUID = 1L;
  private static final String DEFAULT_TEMPLATE_FILE = "@basedesign@/org/openbravo/distributionorder/erpReports/OBDO_DistOrder.jrxml";
  private static final String DEFAULT_OUTPUT_NAME = "OBDO_DistOrder";
  private static final TemplateData[] EMPTY_ARRAY = {};

  @Override
  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    try {
      VariablesSecureApp vars = new VariablesSecureApp(request);
      if (vars.commandIn("DEFAULT")) {
        String strmDistOrderId = vars.getSessionValue("PRINTDISTRIBUTIONORDER.INPOBDODISTORDERID")
            .replace("('", "")
            .replace("')", "");
        printPagePartePDF(response, vars, strmDistOrderId);
      } else
        pageError(response);
    } catch (Exception unexpectedException) {
      log.error("Unexpected exception in Print_DistOrder.doPost ", unexpectedException);
    }
  }

  private void printPagePartePDF(HttpServletResponse response, VariablesSecureApp vars,
      String strmDistOrderId) throws ServletException {
    final HashMap<String, Object> parameters = new HashMap<>(1);
    parameters.put("DOCUMENT_ID", strmDistOrderId);
    String reportTemplateFile = DEFAULT_TEMPLATE_FILE;
    String reportOutputName = DEFAULT_OUTPUT_NAME;

    TemplateData[] templates = getDocumentTypeTemplate(strmDistOrderId);
    if (templates.length > 0) {
      reportTemplateFile = templates[0].templateLocation + "/" + templates[0].templateFilename;
      reportOutputName = templates[0].reportFilename;
    }
    renderJR(vars, response, reportTemplateFile, reportOutputName, "pdf", parameters, null, null,
        false);
  }

  private TemplateData[] getDocumentTypeTemplate(final String strmDistOrderId) {
    TemplateData[] templates;
    try {
      OBContext.setAdminMode(true);
      final DistributionOrder distributionOrder = OBDal.getInstance()
          .get(DistributionOrder.class, strmDistOrderId);
      templates = TemplateData.getDocumentTemplates(this,
          distributionOrder.getDocumentType().getId(), distributionOrder.getOrganization().getId());
    } catch (Exception noTemplateFound) {
      return EMPTY_ARRAY;
    } finally {
      OBContext.restorePreviousMode();
    }
    return templates;
  }

  @Override
  public String getServletInfo() {
    return "Servlet that presents the PrintOptions seeker";
  } // End of getServletInfo() method

}
