/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */
package org.openbravo.distributionorder.event;

import java.math.BigDecimal;

import javax.enterprise.event.Observes;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.distributionorder.DistributionOrder;
import org.openbravo.distributionorder.DistributionOrderLine;
import org.openbravo.erpCommon.utility.OBMessageUtils;

public class DistributionOrderLineEventHandler extends EntityPersistenceEventObserver {
  protected Logger logger = Logger.getLogger(this.getClass());
  private static Entity[] entities = {
      ModelProvider.getInstance().getEntity(DistributionOrderLine.ENTITY_NAME) };

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  public void onSave(@Observes EntityNewEvent event) {
    if (!isValidEvent(event)) {
      return;
    }
    checkDistributionOrderLineQuantity(event);
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }
    checkDistributionOrderLineQuantity(event);
  }

  private void checkDistributionOrderLineQuantity(EntityPersistenceEvent event) {
    final Entity distributionOrderLineEntity = ModelProvider.getInstance()
        .getEntity(DistributionOrderLine.ENTITY_NAME);
    final DistributionOrder distributionOrder = getDistributionOrder(event,
        distributionOrderLineEntity);
    final BigDecimal orderedQuantity = getOrderedQuantity(event, distributionOrderLineEntity);
    final BigDecimal confirmedQuantity = getConfirmedQuantity(event, distributionOrderLineEntity);

    // When the Document is going to be closed, do not check the quantities. It should be
    // possible to set all of them to 0 if nothing has been issued
    if (isGoingToBeClosed(distributionOrder)) {
      return;
    }

    if (quantityIsZeroOrNull(orderedQuantity) && quantityIsZeroOrNull(confirmedQuantity)) {
      if (distributionOrder.isSalesTransaction()) {
        // Not possible to create a new Distribution Order Issue that is not related with any
        // Distribution Order Receipt and has 0 confirmed quantity
        throw new OBException(OBMessageUtils.messageBD("OBDO_ZeroConfirmedQuantity"));
      } else {
        // Not possible to create a new Distribution Order Receipt that is not related with any
        // Distribution Order Issue and has 0 ordered quantity
        throw new OBException(OBMessageUtils.messageBD("OBDO_ZeroOrderedQuantity"));
      }
    }
  }

  private BigDecimal getOrderedQuantity(EntityPersistenceEvent event,
      final Entity distributionOrderLineEntity) {
    return (BigDecimal) event.getCurrentState(
        distributionOrderLineEntity.getProperty(DistributionOrderLine.PROPERTY_ORDEREDQUANTITY));
  }

  private BigDecimal getConfirmedQuantity(EntityPersistenceEvent event,
      final Entity distributionOrderLineEntity) {
    return (BigDecimal) event.getCurrentState(
        distributionOrderLineEntity.getProperty(DistributionOrderLine.PROPERTY_QTYCONFIRMED));
  }

  private DistributionOrder getDistributionOrder(EntityPersistenceEvent event,
      Entity distributionOrderLineEntity) {
    return (DistributionOrder) event.getCurrentState(
        distributionOrderLineEntity.getProperty(DistributionOrderLine.PROPERTY_DISTRIBUTIONORDER));
  }

  private boolean quantityIsZeroOrNull(final BigDecimal orderedQuantity) {
    return orderedQuantity == null || orderedQuantity.compareTo(BigDecimal.ZERO) == 0;
  }

  private boolean isGoingToBeClosed(final DistributionOrder distributionOrder) {
    return StringUtils.equals("CL", distributionOrder.getDocumentAction());
  }
}
