/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.hooks;

import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.distributionorder.DistributionOrder;
import org.openbravo.distributionorder.DistributionOrderLine;
import org.openbravo.distributionorder.erpCommon.utility.ProcessDistributionOrderUtil;

/**
 * Abstract class to be implemented by hooks to be executed after a Distribution Order has been
 * processed.
 *
 */
public abstract class AfterDistributionOrderProcessedHook {

  protected static final String DOCSTATUS_BOOKED = "CO";
  protected static final String DOCSTATUS_REQUESTED = "RE";
  protected static final String DOCSTATUS_CONFIRMED = "CF";
  protected static final String DOCSTATUS_CLOSE = "CL";

  /**
   * Returns the order when the concrete hook will be implemented. A positive value will execute the
   * hook after the core's logic
   */
  public abstract int getOrder();

  /**
   * Returns true if this hooks should be executed for the Distribution Order
   * 
   * @param distributionOrder
   *          The Distribution Order being processed
   * @param docAction
   *          The action to be executed when processing the distribution order:
   *          {@link ProcessDistributionOrderUtil#DOCACTION_BOOK},
   *          {@link ProcessDistributionOrderUtil#DOCACTION_CONFIRM} and
   *          {@link ProcessDistributionOrderUtil#DOCACTION_CLOSE}
   */
  public abstract boolean isValid(final DistributionOrder distributionOrder,
      final String docAction);

  /**
   * This method implements the logic to be run when a Distribution Order has been processed
   * 
   * @param distributionOrder
   *          The Distribution Order being processed
   * @param docAction
   *          The action to be executed when processing the distribution order:
   *          {@link ProcessDistributionOrderUtil#DOCACTION_BOOK},
   *          {@link ProcessDistributionOrderUtil#DOCACTION_CONFIRM} and
   *          {@link ProcessDistributionOrderUtil#DOCACTION_CLOSE}
   */
  public abstract void run(final DistributionOrder distributionOrder, final String docAction);

  /**
   * Returns an ScrollableResult object with Distribution Order lines
   */
  protected ScrollableResults getDistOrderLinesScrollableResult(
      final DistributionOrder distributionOrder) {
    OBCriteria<DistributionOrderLine> linesCriteria = OBDal.getInstance()
        .createCriteria(DistributionOrderLine.class);
    linesCriteria
        .add(Restrictions.eq(DistributionOrderLine.PROPERTY_DISTRIBUTIONORDER, distributionOrder));

    return linesCriteria.scroll(ScrollMode.FORWARD_ONLY);
  }

  /**
   * Returns true when the distribution order is being closed
   */
  protected boolean isBeingClosed(final DistributionOrder distributionOrder,
      final String docAction) {
    return ProcessDistributionOrderUtil.DOCACTION_CLOSE.equals(docAction)
        && isClosed(distributionOrder);
  }

  private boolean isClosed(final DistributionOrder distributionOrder) {
    return distributionOrder.getDocumentStatus().equals(DOCSTATUS_CLOSE);
  }
}
