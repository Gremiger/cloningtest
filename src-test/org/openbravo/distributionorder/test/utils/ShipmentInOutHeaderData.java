/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.test.utils;

import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;

public class ShipmentInOutHeaderData {
  private Organization organization;
  private String docNo;
  private DocumentType docType;
  private Warehouse warehouse;
  private BusinessPartner businessPartner;

  private ShipmentInOutHeaderData(ShipmentInOutHeaderDataBuilder shipmentInOutHeaderDataBuilder) {
    this.organization = shipmentInOutHeaderDataBuilder.organization;
    this.docNo = shipmentInOutHeaderDataBuilder.docNo;
    this.docType = shipmentInOutHeaderDataBuilder.docType;
    this.warehouse = shipmentInOutHeaderDataBuilder.warehouse;
    this.businessPartner = shipmentInOutHeaderDataBuilder.businessPartner;
  }

  public Organization getOrganization() {
    return organization;
  }

  public String getDocNo() {
    return docNo;
  }

  public DocumentType getDocType() {
    return docType;
  }

  public Warehouse getWarehouse() {
    return warehouse;
  }

  public BusinessPartner getBusinessPartner() {
    return businessPartner;
  }

  public static class ShipmentInOutHeaderDataBuilder {
    private Organization organization;
    private String docNo;
    private DocumentType docType;
    private Warehouse warehouse;
    private BusinessPartner businessPartner;

    public ShipmentInOutHeaderDataBuilder() {

    }

    public ShipmentInOutHeaderDataBuilder organizationID(String newOrganizationId) {
      Organization newOrganization = OBDal.getInstance().get(Organization.class, newOrganizationId);
      this.organization = newOrganization;
      return this;
    }

    public ShipmentInOutHeaderDataBuilder documentNo(String newDocNo) {
      this.docNo = newDocNo;
      return this;
    }

    public ShipmentInOutHeaderDataBuilder documentTypeID(String newDocTypeID) {
      DocumentType newDocType = OBDal.getInstance().get(DocumentType.class, newDocTypeID);
      this.docType = newDocType;
      return this;
    }

    public ShipmentInOutHeaderDataBuilder warehouseID(String newWarehouseID) {
      Warehouse newWarehouse = OBDal.getInstance().get(Warehouse.class, newWarehouseID);
      this.warehouse = newWarehouse;
      return this;
    }

    public ShipmentInOutHeaderDataBuilder businessPartnerID(String newBusinessPartnerID) {
      BusinessPartner newBusinessPartner = OBDal.getInstance()
          .get(BusinessPartner.class, newBusinessPartnerID);
      this.businessPartner = newBusinessPartner;
      return this;
    }

    public ShipmentInOutHeaderData build() {
      return new ShipmentInOutHeaderData(this);
    }

  }

}
